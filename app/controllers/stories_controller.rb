class StoriesController < ApplicationController

	def home
		get_stories	
	end

	def index
		get_stories

		render '_index', :layout => false
	end

	def get_stories
		@stories = Story.limit(10)
		# @stories = @stories.by_subject(params[:subject]) if params[:subject].present?
		# @stories = @stories.by_subjects(params[:subjects]) if params[:subjects].present?
		# if params[:subjects].present?
		# 	params[:subjects].each do |subject|
		# 		@stories = @stories.by_subject(subject)
		# 	end
		# end



 		if session[:seen_ids].present?
			unrecent_stories = @stories.where.not(:id => session[:seen_ids])
		
			unrecent_ids = unrecent_stories.pluck(:id)
			if unrecent_ids.count > 0
				session[:seen_ids] = (session[:seen_ids] || []).concat(unrecent_ids).last(50)
				@stories = unrecent_stories
			else 
				session[:seen_ids] = []
			end
		else
			session[:seen_ids] = @stories.pluck(:id)
		end

		puts "@@@@@@@@"
		puts @stories.count
		puts "@@@@@@@@"

	end

	def saved
		render 'saved'
	end

	def show
		@story = Story.find(params[:id])
	end

	def read
		story = Story.find(params[:story_id])
		story.update_attributes!(:reads => (story.reads || 0) + 1)
		respond_to do |format|
		  format.xml { render xml: story }
		  format.json { render json: story }
		end
	end

	def stats
		# Nada
	end

	private
    # Using a private method to encapsulate the permissible parameters is just a good pattern
    # since you'll be able to reuse the same permit list between create and update. Also, you
    # can specialize this method with per-user checking of permissible attributes.
    def story_params
      params.permit(:subject)
    end
end

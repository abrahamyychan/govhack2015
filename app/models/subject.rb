class Subject < ActiveRecord::Base

	has_many :stories_subjects
	has_many :stories, :through => :stories_subjects

end

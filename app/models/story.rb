class Story < ActiveRecord::Base

	has_many :stories_subjects
	has_many :subjects, :through => :stories_subjects

	JSON_MAP = {
		"Title" => :title,
		"URL" => :url,
		"Date" => :published_at,
		"Primary image" => :primary_image_url,
		"Primary image caption" => :primary_image_caption,
		"Station" => :station,
		"State" => :state,
		"Place" => :place,
		"Keywords" => :keywords,
		"MediaRSS URL" => :media_rss_url
	}


	EXAMPLE_JSON = 
	JSON.parse('{
        "Title": "For the love of cats, cake and coffee",
        "URL": "http://www.abc.net.au/local/photos/2014/12/02/4140574.htm",
        "Date": "2/12/2014",
        "Primary image": "http://www.abc.net.au/reslib/201412/r1363494_19180535.jpg",
        "Primary image caption": "Chloe and Tania Mathews run the Alley Cat Cafe.",
        "Primary image rights information": "Copyright: ABC | Source: ABC Local | Byline: Lily Partland",
        "Subjects": "Business, Economics and Finance, Business, Economics and Finance:Small Business, Community and Society, Human Interest:Animals",
        "Station": "ABC Ballarat",
        "State": "VIC",
        "Place": "Ballarat",
        "Keywords": "animals, cats, pets, families, business",
        "Latitude": "-37.5675",
        "Longitude": "143.8509",
        "MediaRSS URL": "http://www.abc.net.au/local/photos/2014/12/02/4140574-mediarss.xml"
    }')



	def self.new_from_json json
		s = Story.where(:title => json["Title"]).first_or_initialize
		JSON_MAP.each do |their_name, our_name|
			s[our_name] = json[their_name]
		end

		json["Subjects"].split(',').each do |subject|
			tiers = subject.split(':').map(&:strip)
			s.subjects << Subject.where(:primary => tiers[0], :secondary => tiers[1], :tertiary => tiers[2]).first_or_create
		end

		s
	end

	def self.import_from_file
		data = JSON.parse(File.read(Rails.root.join("app", "assets","stories.json")))
		data.each do |json|
			Story.new_from_json(json).save!
		end
		Story.count
	end

	scope :by_subject, ->(subject) {
		primary, secondary, tertiary = subject.split(':')
		stories = joins(:subjects).where(:subjects => {:primary => primary})
		stories = stories.where(:subjects => {:secondary => secondary}) if secondary.present?
		stories = stories.where(:subjects => {:tertiary => tertiary}) if tertiary.present?
		stories.uniq
	}

	scope :by_subjects, ->(subjects) {
		stories = joins(:subjects)
		subjects.each do |subject|
			primary, secondary, tertiary = subject.split(':')
			stories = stories.where("primary = :primary", :primary => primary)
		end
		stories
	}	

end

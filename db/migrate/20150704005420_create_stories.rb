class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|

    	t.string :title
    	t.string :url
    	t.date :published_at
    	t.string :primary_image_url
    	t.string :primary_image_caption
    	t.string :primary_image_rights
    	t.string :station
    	t.string :state
    	t.string :place
    	t.text :keywords

    	# lat
    	# long

    	t.string :media_rss_url 	

      t.timestamps null: false
    end
  end
end

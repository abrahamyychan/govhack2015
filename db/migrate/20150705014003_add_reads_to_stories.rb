class AddReadsToStories < ActiveRecord::Migration
	def change
		add_column :stories, :reads, :integer
	end
end

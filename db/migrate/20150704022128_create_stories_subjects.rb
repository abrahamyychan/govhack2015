class CreateStoriesSubjects < ActiveRecord::Migration
  def change
    create_table :stories_subjects do |t|

    	t.references :story, :index => true
    	t.references :subject, :index => true


      t.timestamps null: false
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150705014003) do

  create_table "stories", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.date     "published_at"
    t.string   "primary_image_url"
    t.string   "primary_image_caption"
    t.string   "primary_image_rights"
    t.string   "station"
    t.string   "state"
    t.string   "place"
    t.text     "keywords"
    t.string   "media_rss_url"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "reads"
  end

  create_table "stories_subjects", force: :cascade do |t|
    t.integer  "story_id"
    t.integer  "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "stories_subjects", ["story_id"], name: "index_stories_subjects_on_story_id"
  add_index "stories_subjects", ["subject_id"], name: "index_stories_subjects_on_subject_id"

  create_table "subjects", force: :cascade do |t|
    t.integer  "story_id"
    t.string   "primary"
    t.string   "secondary"
    t.string   "tertiary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
